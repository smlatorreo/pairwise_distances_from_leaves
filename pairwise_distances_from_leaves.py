# USAGE: python3 pairwise_distances_from_leaves.py <tree.newick> > <output.matrix>
from sys import argv
from Bio import Phylo
from itertools import combinations

tree = Phylo.read(argv[1], 'newick')
leaves = sorted([i.name for i in tree.get_terminals()])
pdistances = {pair:tree.distance(pair[0], pair[1]) for pair in combinations(leaves, 2)}

print(*leaves, sep = '\t')
for pos in range(len(leaves)):
    leave = leaves[pos]
    d = []
    for pair in pdistances:
        if leave in pair:
            d.append(pdistances[pair])
    d.insert(pos, 0)
    print(*d, sep = '\t')
