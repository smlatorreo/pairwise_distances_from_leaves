# pairwise_distances_from_leaves

This script takes a Newick formatted tree and returns a symmetric matrix with pairwise distances between all leaves

USAGE:
```bash
python3 pairwise_distances_from_leaves.py <tree.newick> > <output.matrix>
```
